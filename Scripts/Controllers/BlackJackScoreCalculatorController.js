﻿/// <reference path="../Utilities/angular.js" />
/// <reference path="../Global/App.js" />

_g_app.controller("BlackJackScoreCalculatorController", ["$scope", function($scope) {
    $scope.cards = [
        { faceValue: "2", value: 2 },
        { faceValue: "3", value: 3 },
        { faceValue: "4", value: 4 },
        { faceValue: "5", value: 5 },
        { faceValue: "6", value: 6 },
        { faceValue: "7", value: 7 },
        { faceValue: "8", value: 8 },
        { faceValue: "9", value: 9 },
        { faceValue: "10", value: 10 },
        { faceValue: "A", value: 11, isAce: true },
        { faceValue: "K", value: 10 },
        { faceValue: "Q", value: 10 },
        { faceValue: "J", value: 10 }
    ];
    $scope.playingCards = [
        { index: 1, card: $scope.cards[0] },
        { index: 2, card: $scope.cards[0] },
        { index: 3, card: null },
        { index: 4, card: null },
        { index: 5, card: null }
    ];
    $scope.playingCardUpdated = function() {
        $scope.calculate();
    };

    $scope.playingCardCount = 2;
    $scope.playingCardCountOptions = [2, 3, 4, 5];
    $scope.$watch(function(scope) { return scope.playingCardCount; },
        function() {
            $scope.setPlayingCardCount($scope.playingCardCount);
        }
    );
    $scope.setPlayingCardCount = function(playingCardCount) {
        $scope.playingCardCount = playingCardCount;
        for (var iCard = 0; iCard < 5; iCard++) {
            if (iCard < $scope.playingCardCount) {
                if (!$scope.playingCards[iCard].card)
                    $scope.playingCards[iCard].card = $scope.cards[0];
            } else
                $scope.playingCards[iCard].card = null;
        }
        $scope.calculate();
    };

    $scope.score = 0;

    $scope.calculate = function () {
        var aceCount = 0;
        var total = 0;

        for (var iCard in $scope.playingCards) {
            var playingCard = $scope.playingCards[iCard];

            if (!playingCard.card)
                continue;

            total += playingCard.card.value;
            if (playingCard.card.isAce)
                aceCount++;
        }

        if (aceCount > 0 && total > 21) {
            do {
                total -= 10;
                aceCount--;
            } while (aceCount > 0 && total > 21);
        }

        $scope.score = total;
    };

    $scope.calculate();
}]);